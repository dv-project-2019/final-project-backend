package handlers

import (
	"errors"
	"final-project-backend/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type AfternoonPlanHandler struct {
	ans   models.AfternoonPlanService
	ashs  models.AfternoonScheduleService
	items models.ItemService
}

func NewAfternoonPlanHandler(ans models.AfternoonPlanService, ashs models.AfternoonScheduleService, items models.ItemService) *AfternoonPlanHandler {
	return &AfternoonPlanHandler{ans, ashs, items}
}

type AfternoonPlanRes struct {
	ID         uint           `json:"id"`
	Name       string         `json:"name"`
	MealplanID uint           `json:"mealplanId"`
	Items      []ItemResponse `json:"items"`
}

type AfternoonScheduleRes struct {
	ID               uint      `json:"id"`
	CreatedAt        time.Time `json:"created_at"`
	AfternnoonPlanID uint      `json:"afternoonPlanID"`
	ItemID           uint      `json:"itemID"`
}

func (ah *AfternoonPlanHandler) AddItemToMorningplan(c *gin.Context) {
	mealplanIDStr := c.Param("id")
	mealplanID, err := strconv.Atoi(mealplanIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	timeIDStr := c.Query("timeId")
	if timeIDStr == "" {
		Error(c, 404, errors.New("time Id is not found"))
	}
	timeID, err := strconv.Atoi(timeIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	itemIDStr := c.Query("itemId")
	if timeIDStr == "" {
		Error(c, 404, errors.New("time Id is not found"))
	}
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	morning, err := ah.ans.GetPlanByMealplanID(uint(mealplanID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	if morning.ID != uint(timeID) {
		Error(c, 400, errors.New("id is not match"))
		return
	}

	if err := ah.items.UpdateSelected(uint(itemID)); err != nil {
		Error(c, 500, err)
		return
	}

	afternoonSchedule := new(models.AfternoonSchedule)
	afternoonSchedule.AfternoonPlanID = uint(timeID)
	afternoonSchedule.ItemID = uint(itemID)
	if err := ah.ashs.AddMealplanAndItem(afternoonSchedule); err != nil {
		Error(c, 500, err)
		return
	}

	res := AfternoonScheduleRes{
		ID:               afternoonSchedule.ID,
		CreatedAt:        afternoonSchedule.CreatedAt,
		AfternnoonPlanID: afternoonSchedule.AfternoonPlanID,
		ItemID:           afternoonSchedule.ItemID,
	}

	c.JSON(200, res)

}
