package handlers

import "final-project-backend/models"

type AfternoonScheduleHandler struct {
	as models.AfternoonScheduleService
}

func NewAfternoonScheduleHandler(as models.AfternoonScheduleService) *AfternoonScheduleHandler {
	return &AfternoonScheduleHandler{as}
}
