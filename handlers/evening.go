package handlers

import (
	"errors"
	"final-project-backend/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type EveningPlanHandler struct {
	ens   models.EveningPlanService
	eshs  models.EveningScheduleService
	items models.ItemService
}

func NewEveningPlanHandler(ens models.EveningPlanService, eshs models.EveningScheduleService, items models.ItemService) *EveningPlanHandler {
	return &EveningPlanHandler{ens, eshs, items}
}

type EveningPlanRes struct {
	ID         uint           `json:"id"`
	Name       string         `json:"name"`
	MealplanID uint           `json:"mealplanId"`
	Items      []ItemResponse `json:"items"`
}

type EveningScheduleRes struct {
	ID            uint      `json:"id"`
	CreatedAt     time.Time `json:"created_at"`
	EveningPlanID uint      `json:"eveningPlanID"`
	ItemID        uint      `json:"itemID"`
}

func (eh *EveningPlanHandler) AddItemToMorningplan(c *gin.Context) {
	mealplanIDStr := c.Param("id")
	mealplanID, err := strconv.Atoi(mealplanIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	timeIDStr := c.Query("timeId")
	if timeIDStr == "" {
		Error(c, 404, errors.New("time Id is not found"))
	}
	timeID, err := strconv.Atoi(timeIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	itemIDStr := c.Query("itemId")
	if timeIDStr == "" {
		Error(c, 404, errors.New("time Id is not found"))
	}
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	morning, err := eh.ens.GetPlanByMealplanID(uint(mealplanID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	if morning.ID != uint(timeID) {
		Error(c, 400, errors.New("id is not match"))
		return
	}

	if err := eh.items.UpdateSelected(uint(itemID)); err != nil {
		Error(c, 500, err)
		return
	}

	eveningSchedule := new(models.EveningSchedule)
	eveningSchedule.EveningPlanID = uint(timeID)
	eveningSchedule.ItemID = uint(itemID)
	if err := eh.eshs.AddMealplanAndItem(eveningSchedule); err != nil {
		Error(c, 500, err)
		return
	}

	res := EveningScheduleRes{
		ID:            eveningSchedule.ID,
		CreatedAt:     eveningSchedule.CreatedAt,
		EveningPlanID: eveningSchedule.EveningPlanID,
		ItemID:        eveningSchedule.ItemID,
	}

	c.JSON(200, res)

}
