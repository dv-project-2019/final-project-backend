package handlers

import "final-project-backend/models"

type EveningScheduleHandler struct {
	eshs models.EveningScheduleService
}

func NewEveningScheduleHandler(eshs models.EveningScheduleService) *EveningScheduleHandler {
	return &EveningScheduleHandler{eshs}
}
