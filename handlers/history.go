package handlers

import (
	"errors"
	"final-project-backend/context"
	"final-project-backend/models"
	"time"

	"github.com/gin-gonic/gin"
)

type HistoryHandler struct {
	hs    models.HistoryService
	items models.ItemService
}

type HistoryRes struct {
	ID        uint         `json:"id"`
	ItemID    uint         `json:"itemId"`
	UserID    uint         `json:"userId"`
	CreatedAt time.Time    `json:"created`
	Item      ItemResponse `json:"item"`
}

func NewHistoryHandler(hs models.HistoryService, items models.ItemService) *HistoryHandler {
	return &HistoryHandler{hs, items}
}

func (hh HistoryHandler) GetHistoryList(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	histories, err := hh.hs.GetHistoryListByUserID(user.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}

	historyRes := []HistoryRes{}
	for _, history := range histories {
		item, err := hh.items.GetItemsDeletedByItemID(history.ItemID)
		if err != nil {
			Error(c, 404, err)
			return
		}

		image := ItemImageRes{}
		image.ID = item.Image.ID
		image.Filename = item.Image.Filename
		image.itemID = item.Image.ItemID
		image.Url = item.Image.Url

		itemRes := ItemResponse{
			ID:             item.ID,
			ItemName:       item.ItemName,
			Description:    item.Description,
			Calorie:        item.Calorie,
			Amount:         item.Amount,
			Unit:           item.Unit,
			Category:       item.Category,
			ExpirationDate: item.ExpirationDate,
			IsExpired:      item.IsExpired,
			IsSelected:     item.IsSelected,
			UserID:         item.UserID,
			Image:          image,
		}

		historyData := HistoryRes{
			ID:        history.ID,
			ItemID:    history.ItemID,
			UserID:    history.UserID,
			CreatedAt: history.CreatedAt,
			Item:      itemRes,
		}

		historyRes = append(historyRes, historyData)

	}

	c.JSON(200, historyRes)
}
