package handlers

import (
	"final-project-backend/context"
	"final-project-backend/models"
	"fmt"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type ItemHandler struct {
	its        models.ItemService
	hs         models.HistoryService
	itemImages models.ItemImageService
	mnShs      models.MorningScheduleService
	anShs      models.AfternoonScheduleService
	enShs      models.EveningScheduleService
}

type NewItem struct {
	ItemName       string `json:"itemName"`
	Description    string `json:"description"`
	Calorie        uint   `json:"calorie"`
	Amount         uint   `json:"amount"`
	Unit           string `json:"unit"`
	Category       string `json:"category"`
	ExpirationDate int64  `json:"expiration_date"`
	IsExpired      bool   `json:"isExpired"`
	IsSelected     bool   `json:"isSelected"`
	UserID         uint   `json:"userId"`
}

type ItemRes struct {
	ID             uint         `json:"id"`
	ItemName       string       `json:"itemName"`
	Description    string       `json:"description"`
	Calorie        uint         `json:"calorie"`
	Amount         uint         `json:"amount"`
	Unit           string       `json:"unit"`
	Category       string       `json:"category"`
	ExpirationDate time.Time    `json:"expiration_date"`
	IsExpired      bool         `json:"isExpired"`
	IsSelected     bool         `json:"isSelected"`
	UserID         uint         `json:"userId"`
	Image          ItemImageRes `json:"image"`
}

type ItemListRes struct {
	ID uint `json:"id"`
	ItemRes
}

type ItemResponse struct {
	ID             uint         `json:"id"`
	ItemName       string       `json:"itemName"`
	Description    string       `json:"description"`
	Calorie        uint         `json:"calorie"`
	Amount         uint         `json:"amount"`
	Unit           string       `json:"unit"`
	Category       string       `json:"category"`
	ExpirationDate time.Time    `json:"expiration_date"`
	IsExpired      bool         `json:"isExpired"`
	IsSelected     bool         `json:"isSelected"`
	UserID         uint         `json:"userId"`
	Image          ItemImageRes `json:"image"`
}

type UpdateItemExpired struct {
	IsExpired bool `json:"isExpired"`
}

type ItemUpdate struct {
	ItemName       string `json:"itemName"`
	Description    string `json:"description"`
	Calorie        uint   `json:"calorie"`
	Amount         uint   `json:"amount"`
	Unit           string `json:"unit"`
	Category       string `json:"category"`
	ExpirationDate int64  `json:"expiration_date"`
}

type UpdateItemSelected struct {
	IsSelected bool `json:"isSelected"`
}

func NewItemHandler(its models.ItemService, hs models.HistoryService, itemImages models.ItemImageService, mnShs models.MorningScheduleService,
	anShs models.AfternoonScheduleService, enShs models.EveningScheduleService) *ItemHandler {
	return &ItemHandler{its, hs, itemImages, mnShs, anShs, enShs}
}

func (ith *ItemHandler) CreateNewItem(c *gin.Context) {
	newItem := new(NewItem)
	if err := c.BindJSON(newItem); err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}

	item := new(models.Item)
	item.ItemName = newItem.ItemName
	item.Description = newItem.Description
	item.Calorie = newItem.Calorie
	item.Amount = newItem.Amount
	item.Unit = newItem.Unit
	item.Category = newItem.Category
	item.ExpirationDate = time.Unix(newItem.ExpirationDate/1000, 0)
	item.IsExpired = false
	item.IsSelected = false
	item.UserID = user.ID
	if err := ith.its.CreateItem(item); err != nil {
		Error(c, 500, err)
		return
	}

	itemResponse := ItemRes{
		ID:             item.ID,
		ItemName:       item.ItemName,
		Description:    item.Description,
		Calorie:        item.Calorie,
		Amount:         item.Amount,
		Unit:           item.Unit,
		Category:       item.Category,
		ExpirationDate: item.ExpirationDate,
		IsExpired:      item.IsExpired,
		IsSelected:     item.IsSelected,
		UserID:         item.UserID,
	}

	c.JSON(200, itemResponse)
}

func (ith *ItemHandler) GetAllItems(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}

	items, err := ith.its.GetItemsList(user.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}

	itemResponse := []ItemListRes{}
	for _, i := range items {
		image := ItemImageRes{}
		image.ID = i.Image.ID
		image.Filename = i.Image.Filename
		image.itemID = i.Image.ItemID
		image.Url = i.Image.Url

		item := ItemListRes{}
		item.ID = i.ID
		item.ItemName = i.ItemName
		item.Description = i.Description
		item.Calorie = i.Calorie
		item.Amount = i.Amount
		item.Unit = i.Unit
		item.Category = i.Category
		item.ExpirationDate = i.ExpirationDate
		item.IsExpired = i.IsExpired
		item.IsSelected = i.IsSelected
		item.UserID = i.UserID
		item.Image = image
		itemResponse = append(itemResponse, item)
	}

	c.JSON(200, itemResponse)
}

func (ith *ItemHandler) GetItem(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	item, err := ith.its.GetItemById(uint(itemID))
	if err != nil {
		Error(c, 500, err)
		return
	}

	imageRes := ItemImageRes{}
	image, err := ith.itemImages.GetImageByItemID(uint(itemID))
	if err == nil {
		imageRes.ID = image.ID
		imageRes.Filename = image.Filename
		imageRes.itemID = image.ItemID
		imageRes.Url = image.Url
	}

	itemRes := ItemResponse{
		ID:             item.ID,
		ItemName:       item.ItemName,
		Description:    item.Description,
		Calorie:        item.Calorie,
		Amount:         item.Amount,
		Unit:           item.Unit,
		Category:       item.Category,
		ExpirationDate: item.ExpirationDate,
		IsExpired:      item.IsExpired,
		IsSelected:     item.IsSelected,
		UserID:         item.UserID,
		Image:          imageRes,
	}

	c.JSON(200, itemRes)
}

func (ith *ItemHandler) UpdateExpiredItem(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	itemUpdate := new(UpdateItemExpired)
	if err := c.BindJSON(itemUpdate); err != nil {
		Error(c, 400, err)
		return
	}

	item := new(models.Item)
	item.IsExpired = itemUpdate.IsExpired
	if err := ith.its.UpdateExpired(item, uint(itemID)); err != nil {
		Error(c, 500, err)
	}

	c.Status(200)
}

func (ith *ItemHandler) RemoveItem(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}
	history := new(models.History)
	history.ItemID = uint(itemID)
	history.UserID = user.ID
	if err := ith.hs.CreateHistory(history); err != nil {
		Error(c, 500, err)
		return
	}

	if err := ith.mnShs.RemoveItemInMoningSchedule(uint(itemID)); err != nil {
		fmt.Println(err)
	}

	if err := ith.anShs.RemoveItemInAfternoonSchedule(uint(itemID)); err != nil {
		fmt.Println(err)
	}

	if err := ith.enShs.RemoveItemInAfternoonSchedule(uint(itemID)); err != nil {
		fmt.Println(err)
	}

	if err := ith.its.DeleteItem(uint(itemID), user.ID); err != nil {
		Error(c, 500, err)
		return
	}

	c.Status(200)
}

func (ith *ItemHandler) UpdateItem(c *gin.Context) {
	itemIDStr := c.Param("id")
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	itemUpdate := new(ItemUpdate)
	if err := c.BindJSON(itemUpdate); err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}

	item := new(models.Item)
	item.ItemName = itemUpdate.ItemName
	item.Description = itemUpdate.Description
	item.Calorie = itemUpdate.Calorie
	item.Amount = itemUpdate.Amount
	item.Unit = itemUpdate.Unit
	item.Category = itemUpdate.Category
	item.ExpirationDate = time.Unix(itemUpdate.ExpirationDate/1000, 0)

	if err = ith.its.UpdateItem(item, uint(itemID)); err != nil {
		Error(c, 500, err)
		return
	}

	update, err := ith.its.GetItemById(uint(itemID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	res := ItemResponse{
		ID:             update.ID,
		ItemName:       update.ItemName,
		Description:    update.Description,
		Calorie:        update.Calorie,
		Amount:         update.Amount,
		Unit:           update.Unit,
		Category:       update.Category,
		ExpirationDate: update.ExpirationDate,
		IsExpired:      update.IsExpired,
		IsSelected:     item.IsSelected,
		UserID:         update.UserID,
	}

	c.JSON(200, res)

}
