package handlers

import (
	"errors"
	"final-project-backend/context"
	"final-project-backend/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type MealplanHandler struct {
	ms       models.MealplanService
	mns      models.MorningPlanService
	ans      models.AfternoonPlanService
	evns     models.EveningPlanService
	items    models.ItemService
	itemImgs models.ItemImageService
}

func NewMealplanHandler(ms models.MealplanService, mns models.MorningPlanService,
	ans models.AfternoonPlanService, evns models.EveningPlanService, items models.ItemService, itemImgs models.ItemImageService) *MealplanHandler {
	return &MealplanHandler{ms, mns, ans, evns, items, itemImgs}
}

type TimeRes struct {
	ID         uint           `json:"id"`
	Name       string         `json:"name"`
	MealplanID uint           `json:"mealplanId"`
	Items      []ItemResponse `json:"items"`
}

type GetMealplanRes struct {
	ID        uint      `json:"id"`
	UserID    uint      `json:"userId"`
	CreatedAt time.Time `json:"CreatedAt"`
	Time      []TimeRes `json:"time"`
}

type MealplanRes struct {
	ID            uint             `json:"id"`
	UserID        uint             `json:"userId"`
	Date          string           `json:"date"`
	MorningPlan   MorningPlanRes   `json:"morning"`
	AfternoonPlan AfternoonPlanRes `json:"afternoon"`
	EveningPlan   EveningPlanRes   `json:"evening"`
}

type CreateMealplanReq struct {
	Date time.Time `json:"date"`
}

func (mh *MealplanHandler) CreateMealplan(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	currentDate := time.Now().Format("01/02/2006")

	mealplan := new(models.Mealplan)
	mealplan.UserID = user.ID
	mealplan.Date = currentDate
	if err := mh.ms.CreateMealplan(mealplan); err != nil {
		Error(c, 500, err)
		return
	}

	morningPlan := new(models.MorningPlan)
	morningPlan.Name = "Morning"
	morningPlan.MealplanID = mealplan.ID
	if err := mh.mns.CreateMorningPlan(morningPlan); err != nil {
		Error(c, 500, err)
		return
	}

	afternoonPlan := new(models.AfternoonPlan)
	afternoonPlan.Name = "Afternoon"
	afternoonPlan.MealplanID = mealplan.ID
	if err := mh.ans.CreateAfternoonPlan(afternoonPlan); err != nil {
		Error(c, 500, err)
		return
	}

	eveningPlan := new(models.EveningPlan)
	eveningPlan.Name = "Evening"
	eveningPlan.MealplanID = mealplan.ID
	if err := mh.evns.CreateEveningPlan(eveningPlan); err != nil {
		Error(c, 500, err)
		return
	}

	morningRes := MorningPlanRes{
		ID:         morningPlan.ID,
		Name:       morningPlan.Name,
		MealplanID: morningPlan.MealplanID,
	}

	afternoonRes := AfternoonPlanRes{
		ID:         afternoonPlan.ID,
		Name:       afternoonPlan.Name,
		MealplanID: afternoonPlan.MealplanID,
	}

	eveningRes := EveningPlanRes{
		ID:         eveningPlan.ID,
		Name:       eveningPlan.Name,
		MealplanID: eveningPlan.MealplanID,
	}

	response := MealplanRes{
		ID:            mealplan.ID,
		UserID:        mealplan.UserID,
		Date:          mealplan.Date,
		MorningPlan:   morningRes,
		AfternoonPlan: afternoonRes,
		EveningPlan:   eveningRes,
	}

	c.JSON(200, response)
}

func (mh *MealplanHandler) GetMealplanList(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	mealplanlist := mh.ms.GetAllMealplan(user.ID)
	if mealplanlist == nil {
		c.JSON(404, mealplanlist)
		return
	}

	c.JSON(200, mealplanlist)
}

func (mh *MealplanHandler) CheckMealplanCreated(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	currentDate := time.Now().Format("01/02/2006")
	result := mh.ms.CheckMealplanDate(user.ID, currentDate)
	c.JSON(200, gin.H{
		"result": result,
	})
}

func (mh *MealplanHandler) GetMealplan(c *gin.Context) {
	mealplanIDStr := c.Param("id")
	mealplanID, err := strconv.Atoi(mealplanIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	mealplan, err := mh.ms.GetMealplanByID(user.ID, uint(mealplanID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	morningPlan, err := mh.mns.GetPlanByMealplanID(uint(mealplanID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	afternoonPlan, err := mh.ans.GetPlanByMealplanID(uint(mealplanID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	eveningPlan, err := mh.evns.GetPlanByMealplanID(uint(mealplanID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	morningItemRes := []ItemResponse{}
	for _, item := range morningPlan.Items {
		imageRes := ItemImageRes{}
		image, err := mh.itemImgs.GetImageByItemID(item.ID)
		if err == nil {
			imageRes.ID = image.ID
			imageRes.Filename = image.Filename
			imageRes.itemID = image.ItemID
			imageRes.Url = image.Url
		}

		morningItemRes = append(morningItemRes, ItemResponse{
			ID:             item.ID,
			ItemName:       item.ItemName,
			Description:    item.Description,
			Calorie:        item.Calorie,
			Amount:         item.Amount,
			Unit:           item.Unit,
			Category:       item.Category,
			ExpirationDate: item.ExpirationDate,
			IsExpired:      item.IsExpired,
			IsSelected:     item.IsSelected,
			UserID:         item.UserID,
			Image:          imageRes,
		})
	}

	afternoonItemRes := []ItemResponse{}
	for _, item := range afternoonPlan.Items {
		imageRes := ItemImageRes{}
		image, err := mh.itemImgs.GetImageByItemID(item.ID)
		if err == nil {
			imageRes.ID = image.ID
			imageRes.Filename = image.Filename
			imageRes.itemID = image.ItemID
			imageRes.Url = image.Url
		}

		afternoonItemRes = append(afternoonItemRes, ItemResponse{
			ID:             item.ID,
			ItemName:       item.ItemName,
			Description:    item.Description,
			Calorie:        item.Calorie,
			Amount:         item.Amount,
			Unit:           item.Unit,
			Category:       item.Category,
			ExpirationDate: item.ExpirationDate,
			IsExpired:      item.IsExpired,
			IsSelected:     item.IsSelected,
			UserID:         item.UserID,
			Image:          imageRes,
		})
	}

	eveningItemRes := []ItemResponse{}
	for _, item := range eveningPlan.Items {
		imageRes := ItemImageRes{}
		image, err := mh.itemImgs.GetImageByItemID(item.ID)
		if err == nil {
			imageRes.ID = image.ID
			imageRes.Filename = image.Filename
			imageRes.itemID = image.ItemID
			imageRes.Url = image.Url
		}

		eveningItemRes = append(eveningItemRes, ItemResponse{
			ID:             item.ID,
			ItemName:       item.ItemName,
			Description:    item.Description,
			Calorie:        item.Calorie,
			Amount:         item.Amount,
			Unit:           item.Unit,
			Category:       item.Category,
			ExpirationDate: item.ExpirationDate,
			IsExpired:      item.IsExpired,
			IsSelected:     item.IsSelected,
			UserID:         item.UserID,
			Image:          imageRes,
		})
	}

	morningRes := TimeRes{
		ID:         morningPlan.ID,
		Name:       morningPlan.Name,
		MealplanID: morningPlan.MealplanID,
		Items:      morningItemRes,
	}

	afternoonRes := TimeRes{
		ID:         afternoonPlan.ID,
		Name:       afternoonPlan.Name,
		MealplanID: afternoonPlan.MealplanID,
		Items:      afternoonItemRes,
	}

	eveningRes := TimeRes{
		ID:         eveningPlan.ID,
		Name:       eveningPlan.Name,
		MealplanID: eveningPlan.MealplanID,
		Items:      eveningItemRes,
	}

	time := []TimeRes{}
	time = append(time, morningRes)
	time = append(time, afternoonRes)
	time = append(time, eveningRes)

	mealplanRes := GetMealplanRes{
		ID:        mealplan.ID,
		UserID:    mealplan.UserID,
		CreatedAt: mealplan.CreatedAt,
		Time:      time,
	}

	c.JSON(200, mealplanRes)
}
