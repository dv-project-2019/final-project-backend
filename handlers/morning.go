package handlers

import (
	"errors"
	"final-project-backend/models"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type MorningPlanHandler struct {
	mps   models.MorningPlanService
	mshs  models.MorningScheduleService
	items models.ItemService
}

type MorningPlanRes struct {
	ID         uint           `json:"id"`
	Name       string         `json:"name"`
	MealplanID uint           `json:"mealplanId"`
	Items      []ItemResponse `json:"items"`
}

type MorningScheduleRes struct {
	ID            uint      `json:"id"`
	CreatedAt     time.Time `json:"created_at"`
	MorningPlanID uint      `json:"morningPlanID"`
	ItemID        uint      `json:"itemID"`
}

func NewMorningPlanHandler(mps models.MorningPlanService, mshs models.MorningScheduleService, items models.ItemService) *MorningPlanHandler {
	return &MorningPlanHandler{mps, mshs, items}
}

func (mh *MorningPlanHandler) AddItemToMorningplan(c *gin.Context) {
	mealplanIDStr := c.Param("id")
	mealplanID, err := strconv.Atoi(mealplanIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	timeIDStr := c.Query("timeId")
	if timeIDStr == "" {
		Error(c, 404, errors.New("time Id is not found"))
	}
	timeID, err := strconv.Atoi(timeIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	itemIDStr := c.Query("itemId")
	if timeIDStr == "" {
		Error(c, 404, errors.New("time Id is not found"))
	}
	itemID, err := strconv.Atoi(itemIDStr)
	if err != nil {
		Error(c, 400, err)
		return
	}

	morning, err := mh.mps.GetPlanByMealplanID(uint(mealplanID))
	if err != nil {
		Error(c, 404, err)
		return
	}

	if morning.ID != uint(timeID) {
		Error(c, 400, errors.New("id is not match"))
		return
	}

	if err := mh.items.UpdateSelected(uint(itemID)); err != nil {
		Error(c, 500, err)
		return
	}

	morningSchedule := new(models.MorningSchedule)
	morningSchedule.MorningPlanID = uint(timeID)
	morningSchedule.ItemID = uint(itemID)
	if err := mh.mshs.AddMealplanAndItem(morningSchedule); err != nil {
		Error(c, 500, err)
		return
	}

	res := MorningScheduleRes{
		ID:            morningSchedule.ID,
		CreatedAt:     morningSchedule.CreatedAt,
		MorningPlanID: morningSchedule.MorningPlanID,
		ItemID:        morningSchedule.ItemID,
	}

	c.JSON(200, res)

}
