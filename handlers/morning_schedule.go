package handlers

import "final-project-backend/models"

type MorningScheduleHandler struct {
	ms models.MorningScheduleService
}

func NewMorningScheduleHandler(ms models.MorningScheduleService) *MorningScheduleHandler {
	return &MorningScheduleHandler{ms}
}
