package handlers

import (
	"errors"
	"final-project-backend/context"
	"final-project-backend/models"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us     models.UserService
	usImgs models.UserImageService
}

func NewUserHandler(us models.UserService, usImgs models.UserImageService) *UserHandler {
	return &UserHandler{us, usImgs}
}

type SignUpReq struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Dob       int64  `json:"dob"`
}

type SignUpRes struct {
	Email     string    `json:"email"`
	Firstname string    `json:"firstname"`
	Lastname  string    `json:"lastname"`
	Dob       time.Time `json:"dob"`
}

type UserProfileRes struct {
	ID        uint      `json:"id"`
	Email     string    `json:"email"`
	Firstname string    `json:"firstname"`
	Lastname  string    `json:"lastname"`
	Dob       time.Time `json:"dob"`
	UserImage ImageRes  `json:"image"`
}

type LoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UpdateReq struct {
	Email     string `json:"email"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Dob       int64  `json:"dob"`
}

func (uh *UserHandler) CheckEmailAvailability(c *gin.Context) {
	email := c.Query("email")
	if email == "" {
		return
	}
	result, err := uh.us.FindEmailAvailability(email)
	if err != nil {
		Error(c, 404, err)
		return
	}

	c.JSON(200, gin.H{
		"result": result,
	})
}

func (uh *UserHandler) Signup(c *gin.Context) {
	req := new(SignUpReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}

	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	user.Firstname = req.Firstname
	user.Lastname = req.Lastname
	user.Dob = time.Unix(req.Dob/1000, 0)
	if err := uh.us.CreateUser(user); err != nil {
		Error(c, 500, err)
		return
	}

	res := SignUpRes{
		Email:     user.Email,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
		Dob:       user.Dob,
	}

	c.JSON(201, res)
}

func (uh *UserHandler) Login(c *gin.Context) {
	loginReq := new(LoginReq)
	if err := c.BindJSON(loginReq); err != nil {
		Error(c, 404, err)
		return
	}

	user := new(models.User)
	user.Email = loginReq.Email
	user.Password = loginReq.Password
	token, err := uh.us.UserLogin(user)
	if err != nil {
		Error(c, 401, err)
		return
	}

	c.JSON(201, gin.H{
		"token": token,
	})
}

func (uh *UserHandler) GetProfile(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "invalid user",
		})
		return
	}
	imgRes := ImageRes{}
	image, err := uh.usImgs.FindUserImageByID(user.ID)
	if err == nil {
		imgRes.ID = image.ID
		imgRes.itemID = image.UserID
		imgRes.Filename = image.Filename
		imgRes.Url = image.Url
	}

	userResponse := UserProfileRes{
		ID:        user.ID,
		Email:     user.Email,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
		Dob:       user.Dob,
		UserImage: imgRes,
	}

	c.JSON(200, userResponse)
}

func (uh *UserHandler) Logout(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	if err := uh.us.UpdateToken(user.ID); err != nil {
		c.JSON(500, gin.H{
			"message": err,
		})
		return
	}

	c.Status(200)
}

func (uh *UserHandler) UpdateUserProfile(c *gin.Context) {
	userReq := UpdateReq{}
	if err := c.BindJSON(&userReq); err != nil {
		c.JSON(404, gin.H{
			"message": err,
		})
		return
	}
	fmt.Printf("%v : ", userReq)
	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	user.Firstname = userReq.Firstname
	user.Lastname = userReq.Lastname
	user.Email = userReq.Email
	user.Dob = time.Unix(userReq.Dob/1000, 0)
	if err := uh.us.Update(user); err != nil {
		Error(c, 500, err)
		return
	}

	c.JSON(200, UserProfileRes{
		ID:        user.ID,
		Email:     user.Email,
		Firstname: user.Firstname,
		Lastname:  user.Lastname,
		Dob:       user.Dob,
	})

}
