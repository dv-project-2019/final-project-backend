package handlers

import (
	"errors"
	"final-project-backend/context"
	"final-project-backend/models"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
)

type UserImageHandler struct {
	usImgs models.UserImageService
}

func NewUserImageHandler(usImgs models.UserImageService) *UserImageHandler {
	return &UserImageHandler{usImgs}
}

type ImageRes struct {
	ID       uint   `json:"id"`
	itemID   uint   `json:"itemId"`
	Filename string `json:"filename"`
	Url      string `json:"url"`
}

func (usImgh *UserImageHandler) Uploadimage(c *gin.Context) {
	user := context.GetUser(c)
	if user == nil {
		Error(c, 401, errors.New("invalid user"))
		return
	}

	result := usImgh.usImgs.CheckImageCreated(user.ID)
	if result {
		err := usImgh.usImgs.DeleteImage(user.ID)
		if err != nil {
			Error(c, 500, err)
			return
		}
	}

	form, err := c.MultipartForm()
	if err != nil {
		Error(c, 400, err)
		return
	}

	imageUpload, err := usImgh.usImgs.CreateUserImage(form.File["userImage"][0], user.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}

	userIDStr := strconv.FormatUint(uint64(user.ID), 10)
	res := new(ImageRes)
	res.ID = imageUpload.ID
	res.Filename = filepath.Join(models.UploadPath, userIDStr, imageUpload.Filename)
	res.Url = imageUpload.Url

	c.JSON(201, res)
}
