package main

import (
	"final-project-backend/config"
	"final-project-backend/handlers"
	"final-project-backend/hash"
	"final-project-backend/models"
	"final-project-backend/mv"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true)
	}

	err = models.AutoMigrate(db)
	if err != nil {
		log.Fatal(err)
	}

	hmac := hash.NewHMAC(conf.HMACKey)
	users := models.NewUserService(db, hmac)
	items := models.NewItemService(db)
	historys := models.NewHistoryService(db)
	itemImages := models.NewItemImageService(db)
	mealplans := models.NewMealplanService(db)
	morningplans := models.NewMorningPlanService(db)
	afternoon := models.NewAfternoonPlanService(db)
	evenings := models.NewEveningPlanService(db)
	morningSchedules := models.NewMorningScheduleService(db)
	afternoonSchedules := models.NewAfternoonScheduleService(db)
	eveningSchedules := models.NewEveningScheduleService(db)
	userImgs := models.NewUserImageService(db)

	userh := handlers.NewUserHandler(users, userImgs)
	itemh := handlers.NewItemHandler(items, historys, itemImages, morningSchedules, afternoonSchedules, eveningSchedules)
	itemImageh := handlers.NewItemImageHandler(itemImages, items)
	historyh := handlers.NewHistoryHandler(historys, items)
	mealplanh := handlers.NewMealplanHandler(mealplans, morningplans, afternoon, evenings, items, itemImages)
	morningplanh := handlers.NewMorningPlanHandler(morningplans, morningSchedules, items)
	afternoonplanh := handlers.NewAfternoonPlanHandler(afternoon, afternoonSchedules, items)
	eveningplanh := handlers.NewEveningPlanHandler(evenings, eveningSchedules, items)
	userImgh := handlers.NewUserImageHandler(userImgs)

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = []string{"Authorization", "Content-Type", "Origin", "Content-Length"}
	router.Use(cors.New(config))

	router.Static("/image", "./upload")
	router.Static("/userImage", "./upload/userImages")

	auth := router.Group("/auth")
	{
		auth.POST("/signup", userh.Signup)
		auth.GET("/checkEmailAvailability", userh.CheckEmailAvailability)
		auth.POST("/login", userh.Login)
	}

	user := router.Group("/")
	user.Use(mv.RequireUser(users))
	{
		user.POST("/item", itemh.CreateNewItem)
		user.GET("/items", itemh.GetAllItems)
		user.GET("/item/:id", itemh.GetItem)
		user.PATCH("/item/:id", itemh.UpdateExpiredItem)
		user.DELETE("/item/:id", itemh.RemoveItem)
		user.PATCH("/item/:id/update", itemh.UpdateItem)
		user.POST("/item/:id/image", itemImageh.CreateItemImage)
		user.GET("/history", historyh.GetHistoryList)
		user.GET("/mealplans", mealplanh.GetMealplanList)
		user.GET("/mealplan/:id", mealplanh.GetMealplan)
		user.GET("/checkMealplanCreated", mealplanh.CheckMealplanCreated)
		user.POST("/mealplan", mealplanh.CreateMealplan)
		user.POST("/mealplan/:id/morning", morningplanh.AddItemToMorningplan)
		user.POST("/mealplan/:id/afternoon", afternoonplanh.AddItemToMorningplan)
		user.POST("/mealplan/:id/evening", eveningplanh.AddItemToMorningplan)

		admin := user.Group("/user")
		{
			admin.GET("/profile", userh.GetProfile)
			admin.POST("/logout", userh.Logout)
			admin.PATCH("/profile", userh.UpdateUserProfile)
			admin.PATCH("/uploadimage", userImgh.Uploadimage)
		}
	}

	router.Run()
}
