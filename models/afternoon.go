package models

import (
	"github.com/jinzhu/gorm"
)

type AfternoonPlan struct {
	gorm.Model
	Name       string `gorm:"not null", json:"name"`
	MealplanID uint   `gorm:"not null", json:"mealplanId"`
	Items      []Item `json:"items`
}

type AfternoonPlanGorm struct {
	db *gorm.DB
}

type AfternoonPlanService interface {
	CreateAfternoonPlan(plan *AfternoonPlan) error
	GetPlanByMealplanID(mealplanID uint) (*AfternoonPlan, error)
}

func NewAfternoonPlanService(db *gorm.DB) AfternoonPlanService {
	return &AfternoonPlanGorm{db}
}

func (an *AfternoonPlanGorm) CreateAfternoonPlan(plan *AfternoonPlan) error {
	return an.db.Create(&plan).Error
}

func (an *AfternoonPlanGorm) GetPlanByMealplanID(mealplanID uint) (*AfternoonPlan, error) {
	plan := AfternoonPlan{}
	if err := an.db.Where("mealplan_id = ?", mealplanID).First(&plan).Error; err != nil {
		return nil, err
	}

	schedule := []AfternoonSchedule{}
	if err := an.db.Where("afternoon_plan_id = ?", plan.ID).Find(&schedule).Error; err != nil {
		return nil, err
	}

	items := []Item{}
	for _, sd := range schedule {
		item := Item{}
		err := an.db.Where("id = ?", sd.ItemID).First(&item).Error
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}

	plan.Items = items
	return &plan, nil
}
