package models

import (
	"github.com/jinzhu/gorm"
)

type AfternoonSchedule struct {
	gorm.Model
	AfternoonPlanID uint `json:"afternoonPlanID"`
	ItemID          uint `json:"itemID"`
}

type AfternoonScheduleGorm struct {
	db *gorm.DB
}

type AfternoonScheduleService interface {
	AddMealplanAndItem(data *AfternoonSchedule) error
	RemoveItemInAfternoonSchedule(itemID uint) error
}

func NewAfternoonScheduleService(db *gorm.DB) AfternoonScheduleService {
	return &AfternoonScheduleGorm{db}
}

func (ag *AfternoonScheduleGorm) AddMealplanAndItem(data *AfternoonSchedule) error {
	return ag.db.Create(&data).Error
}

func (ag *AfternoonScheduleGorm) RemoveItemInAfternoonSchedule(itemID uint) error {
	data := AfternoonSchedule{}
	if err := ag.db.Where("item_id = ?", itemID).First(&data).Error; err != nil {
		return err
	}

	if err := ag.db.Delete(&data).Error; err != nil {
		return err
	}

	return nil
}
