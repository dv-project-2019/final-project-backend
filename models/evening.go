package models

import (
	"github.com/jinzhu/gorm"
)

type EveningPlan struct {
	gorm.Model
	Name       string `gorm:"not null", json:"name"`
	MealplanID uint   `gorm:"not null", json:"mealplanId"`
	Items      []Item `json:"items`
}

type EveningPlanGorm struct {
	db *gorm.DB
}

type EveningPlanService interface {
	CreateEveningPlan(plan *EveningPlan) error
	GetPlanByMealplanID(mealplanID uint) (*EveningPlan, error)
}

func NewEveningPlanService(db *gorm.DB) EveningPlanService {
	return &EveningPlanGorm{db}
}

func (eng *EveningPlanGorm) CreateEveningPlan(plan *EveningPlan) error {
	return eng.db.Create(&plan).Error
}

func (eng *EveningPlanGorm) GetPlanByMealplanID(mealplanID uint) (*EveningPlan, error) {
	plan := EveningPlan{}
	if err := eng.db.Where("mealplan_id = ?", mealplanID).First(&plan).Error; err != nil {
		return nil, err
	}

	schedule := []EveningSchedule{}
	if err := eng.db.Where("evening_plan_id = ?", plan.ID).Find(&schedule).Error; err != nil {
		return nil, err
	}

	items := []Item{}
	for _, sd := range schedule {
		item := Item{}
		err := eng.db.Where("id = ?", sd.ItemID).First(&item).Error
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}

	plan.Items = items

	return &plan, nil
}
