package models

import (
	"github.com/jinzhu/gorm"
)

type EveningSchedule struct {
	gorm.Model
	EveningPlanID uint `json:"eveningPlanID"`
	ItemID        uint `json:"itemID"`
}

type EveningScheduleGorm struct {
	db *gorm.DB
}

type EveningScheduleService interface {
	AddMealplanAndItem(data *EveningSchedule) error
	RemoveItemInAfternoonSchedule(itemID uint) error
}

func NewEveningScheduleService(db *gorm.DB) EveningScheduleService {
	return &EveningScheduleGorm{db}
}

func (eng *EveningScheduleGorm) AddMealplanAndItem(data *EveningSchedule) error {
	return eng.db.Create(&data).Error
}

func (eng *EveningScheduleGorm) RemoveItemInAfternoonSchedule(itemID uint) error {
	data := EveningSchedule{}
	if err := eng.db.Where("item_id = ?", itemID).First(&data).Error; err != nil {
		return err
	}

	if err := eng.db.Delete(&data).Error; err != nil {
		return err
	}

	return nil
}
