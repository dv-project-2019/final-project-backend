package models

import "github.com/jinzhu/gorm"

type History struct {
	gorm.Model
	ItemID uint `gorm:"not null", json:"itemId"`
	UserID uint `gorm:"not null", json:"userId"`
}

type HistoryGorm struct {
	db *gorm.DB
}

type HistoryService interface {
	CreateHistory(historyReq *History) error
	GetHistoryListByUserID(userID uint) ([]History, error)
}

func NewHistoryService(db *gorm.DB) HistoryService {
	return &HistoryGorm{db}
}

func (hg *HistoryGorm) CreateHistory(historyReq *History) error {
	return hg.db.Create(&historyReq).Error
}

func (hg *HistoryGorm) GetHistoryListByUserID(userID uint) ([]History, error) {
	histories := []History{}
	if err := hg.db.Where("user_id = ?", userID).Find(&histories).Error; err != nil {
		return nil, err
	}
	return histories, nil
}
