package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

type Item struct {
	gorm.Model
	ItemName       string    `gorm:"not null", json:"itemName"`
	Description    string    `json:"description"`
	Calorie        uint      `gorm:"not null", json:"calorie"`
	Amount         uint      `gorm:"not null", json:"amount"`
	Unit           string    `gorm:"not null", json:"unit"`
	Category       string    `gorm:"not null", json:"category"`
	ExpirationDate time.Time `gorm:"not null", json:"expiration_date"`
	IsExpired      bool      `gorm:"not null", json:"isExpired"`
	IsSelected     bool      `gorm:"not null", json:"isSelected"`
	UserID         uint      `gorm:"not null", json:"userId"`
	Image          ItemImage `json:"image"`
}

type ItemGorm struct {
	db *gorm.DB
}

type ItemService interface {
	CreateItem(itemReq *Item) error
	GetItemsList(userID uint) ([]Item, error)
	GetItemById(itemID uint) (*Item, error)
	UpdateExpired(itemUpdate *Item, itemID uint) error
	DeleteItem(itemID uint, userID uint) error
	UpdateItem(itemReq *Item, itemID uint) error
	GetItemsDeletedByItemID(itemID uint) (*Item, error)
	UpdateSelected(itemID uint) error
}

func NewItemService(db *gorm.DB) ItemService {
	return &ItemGorm{db}
}

func (itemg *ItemGorm) CreateItem(itemReq *Item) error {
	return itemg.db.Create(&itemReq).Error
}

func (itemg *ItemGorm) GetItemsList(userID uint) ([]Item, error) {
	items := []Item{}
	if err := itemg.db.Order("created_at DESC").Where("user_id = ?", userID).Find(&items).Error; err != nil {
		return nil, err
	}

	for i := 0; i < len(items); i++ {
		image := ItemImage{}
		err := itemg.db.Where("item_id = ?", items[i].ID).First(&image).Error
		if err != nil {
			items[i].Image = image
		}
		items[i].Image = image
	}
	return items, nil
}

func (itemg *ItemGorm) GetItemById(itemID uint) (*Item, error) {
	item := Item{}
	if err := itemg.db.Where("id = ?", itemID).First(&item).Error; err != nil {
		return nil, errors.New("Item is not found")
	}
	return &item, nil
}

func (itemg *ItemGorm) UpdateExpired(itemUpdate *Item, itemID uint) error {
	item, err := itemg.GetItemById(itemID)
	if err != nil {
		return err
	}

	if err := itemg.db.Model(&item).Update("is_expired", !itemUpdate.IsExpired).Error; err != nil {
		return errors.New("cannot update item expired")
	}
	return nil
}

func (itemg *ItemGorm) DeleteItem(itemID uint, userID uint) error {
	item, err := itemg.GetItemById(itemID)
	if err != nil {
		return err
	}

	if item.UserID != userID {
		return errors.New("user id is not match")
	}

	if err := itemg.db.Delete(&item).Error; err != nil {
		return errors.New("Delete item is not success")
	}
	return nil
}

func (itemg *ItemGorm) UpdateItem(itemReq *Item, itemID uint) error {
	item, err := itemg.GetItemById(itemID)
	if err != nil {
		return err
	}

	if itemReq.ItemName != "" {
		item.ItemName = itemReq.ItemName
	}

	if itemReq.Description == "" || itemReq.Description != "" {
		item.Description = itemReq.Description
	}

	if itemReq.Amount >= 0 {
		item.Amount = itemReq.Amount
	}

	if itemReq.Calorie >= 0 {
		item.Calorie = itemReq.Calorie
	}

	if itemReq.Unit != "" {
		item.Unit = itemReq.Unit
	}

	if itemReq.Category != "" {
		item.Category = itemReq.Category
	}

	if !itemReq.ExpirationDate.IsZero() {
		item.ExpirationDate = itemReq.ExpirationDate
	}

	if err := itemg.db.Save(&item).Error; err != nil {
		return err
	}
	return nil
}

func (itemg *ItemGorm) GetItemsDeletedByItemID(itemID uint) (*Item, error) {
	item := Item{}
	if err := itemg.db.Unscoped().Where("id = ?", itemID).First(&item).Error; err != nil {
		return nil, errors.New("Item is not found")
	}

	image := ItemImage{}
	err := itemg.db.Where("item_id = ?", item.ID).First(&image).Error
	if err != nil {
		item.Image = image
	}
	item.Image = image

	return &item, nil
}

func (itemg *ItemGorm) UpdateSelected(itemID uint) error {
	item, err := itemg.GetItemById(itemID)
	if err != nil {
		return err
	}

	if item.IsSelected == true {
		return errors.New("Item is selected")
	}

	if err := itemg.db.Model(&item).Update("is_selected", !item.IsSelected).Error; err != nil {
		return errors.New("cannot update item selected")
	}

	return nil
}
