package models

import (
	"errors"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

const UploadPath = "upload"
const Port = "https://eatontimeapi.jumpingcrab.com"

type ItemImage struct {
	gorm.Model
	ItemID   uint   `gorm:"not null", json:"itemId" `
	Filename string `gorm:"not null", json:"filename" `
	Url      string `gorm:"not null", json:"url"`
}

type ItemImageGorm struct {
	db *gorm.DB
}

type ItemImageService interface {
	CreateImage(file *multipart.FileHeader, itemID uint) (*ItemImage, error)
	GetImageByItemID(itemID uint) (*ItemImage, error)
}

func NewItemImageService(db *gorm.DB) ItemImageService {
	return &ItemImageGorm{db}
}

func RandStringRunes(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func saveFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		log.Printf("saveFile - open file error: %v\n", err)
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		log.Printf("saveFile - create destination file error: %v\n", err)
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	if err != nil {
		log.Printf("saveFile - copy file error: %v\n", err)
	}
	return err
}

func (img *ItemImage) FilePath() string {
	idStr := strconv.FormatUint(uint64(img.ItemID), 10)
	return filepath.Join(UploadPath, idStr, img.Filename)
}

func (itimg *ItemImageGorm) CreateImage(file *multipart.FileHeader, itemID uint) (*ItemImage, error) {
	itemIDStr := strconv.FormatUint(uint64(itemID), 10)
	direction := filepath.Join(UploadPath, itemIDStr)
	if err := os.MkdirAll(direction, os.ModePerm); err != nil {
		return nil, err
	}

	tx := itimg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	randomFilename := RandStringRunes(24)
	image := ItemImage{
		ItemID:   itemID,
		Filename: randomFilename,
		Url:      Port + "/image/" + itemIDStr + "/" + randomFilename,
	}
	if err := tx.Create(&image).Error; err != nil {
		log.Printf("create image error: %v\n", err)
		tx.Rollback()
		return nil, err
	}

	if err := saveFile(file, image.FilePath()); err != nil {
		tx.Rollback()
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}

	return &image, nil
}

func (itimg *ItemImageGorm) GetImageByItemID(itemID uint) (*ItemImage, error) {
	image := ItemImage{}
	err := itimg.db.Where("item_id = ?", itemID).First(&image).Error
	if err != nil {
		return nil, errors.New("Image is not found")
	}

	return &image, nil
}
