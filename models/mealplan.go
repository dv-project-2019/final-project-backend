package models

import (
	"errors"
	"fmt"

	"github.com/jinzhu/gorm"
)

type Mealplan struct {
	gorm.Model
	UserID uint   `gorm:"not null", json:"userId"`
	Date   string `gorm:"not null", json:"date"`
}

type MealplanGorm struct {
	db *gorm.DB
}

type MealplanService interface {
	CreateMealplan(mealplan *Mealplan) error
	GetAllMealplan(userID uint) []Mealplan
	CheckMealplanDate(userID uint, date string) bool
	GetMealplanByID(userID uint, mealplanID uint) (*Mealplan, error)
}

func NewMealplanService(db *gorm.DB) MealplanService {
	return &MealplanGorm{db}
}

func (mg *MealplanGorm) CreateMealplan(mealplan *Mealplan) error {
	return mg.db.Create(&mealplan).Error
}

func (mg *MealplanGorm) GetAllMealplan(userID uint) []Mealplan {
	mealplanList := []Mealplan{}
	if err := mg.db.Order("created_at DESC").Where("user_id = ?", userID).Find(&mealplanList).Error; err != nil {
		return nil
	}

	return mealplanList
}

func (mg *MealplanGorm) CheckMealplanDate(userID uint, date string) bool {
	mealplanList := mg.GetAllMealplan(userID)
	if mealplanList == nil {
		return true
	}

	for _, mealplan := range mealplanList {
		createdDate := mealplan.Date
		fmt.Println("current: ==> ", date)
		fmt.Println("created: ==> ", createdDate)
		if date == createdDate {
			return false
		}
	}
	return true

}

func (mg *MealplanGorm) GetMealplanByID(userID uint, mealplanID uint) (*Mealplan, error) {
	mealplan := Mealplan{}
	if err := mg.db.Where("id = ?", mealplanID).First(&mealplan).Error; err != nil {
		return nil, err
	}
	if mealplan.UserID != userID {
		return nil, errors.New("invalid user")
	}

	return &mealplan, nil
}
