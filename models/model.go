package models

import "github.com/jinzhu/gorm"

func AutoMigrate(db *gorm.DB) error {
	return db.AutoMigrate(
		&Item{},
		&User{},
		&History{},
		&ItemImage{},
		&Mealplan{},
		&MorningPlan{},
		&AfternoonPlan{},
		&EveningPlan{},
		&MorningSchedule{},
		&AfternoonSchedule{},
		&EveningSchedule{},
		&UserImage{},
	).Error
}
