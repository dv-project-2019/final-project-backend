package models

import (
	"github.com/jinzhu/gorm"
)

type MorningPlan struct {
	gorm.Model
	Name       string            `gorm:"not null", json:"name"`
	MealplanID uint              `gorm:"not null", json:"mealplanId"`
	Schedule   []MorningSchedule `json:"morning_schedule"`
	Items      []Item            `json:"items"`
}

type MorningPlanGorm struct {
	db *gorm.DB
}

type MorningPlanService interface {
	CreateMorningPlan(plan *MorningPlan) error
	GetPlanByMealplanID(mealplanID uint) (*MorningPlan, error)
}

func NewMorningPlanService(db *gorm.DB) MorningPlanService {
	return &MorningPlanGorm{db}
}

func (mg *MorningPlanGorm) CreateMorningPlan(plan *MorningPlan) error {
	return mg.db.Create(&plan).Error
}

func (mg *MorningPlanGorm) GetPlanByMealplanID(mealplanID uint) (*MorningPlan, error) {
	plan := MorningPlan{}
	if err := mg.db.Where("mealplan_id = ?", mealplanID).First(&plan).Error; err != nil {
		return nil, err
	}

	schedule := []MorningSchedule{}
	if err := mg.db.Where("morning_plan_id = ?", plan.ID).Find(&schedule).Error; err != nil {
		return nil, err
	}

	items := []Item{}
	for _, sd := range schedule {
		item := Item{}
		err := mg.db.Where("id = ?", sd.ItemID).First(&item).Error
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}

	plan.Items = items
	return &plan, nil
}
