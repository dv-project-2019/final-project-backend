package models

import (
	"github.com/jinzhu/gorm"
)

type MorningSchedule struct {
	gorm.Model
	MorningPlanID uint `json:"morningPlanID"`
	ItemID        uint `json:"itemID"`
}

type MorningScheduleGorm struct {
	db *gorm.DB
}

type MorningScheduleService interface {
	AddMealplanAndItem(data *MorningSchedule) error
	RemoveItemInMoningSchedule(itemID uint) error
}

func NewMorningScheduleService(db *gorm.DB) MorningScheduleService {
	return &MorningScheduleGorm{db}
}

func (mshg *MorningScheduleGorm) AddMealplanAndItem(data *MorningSchedule) error {
	return mshg.db.Create(&data).Error
}

func (mshg *MorningScheduleGorm) RemoveItemInMoningSchedule(itemID uint) error {
	data := MorningSchedule{}
	if err := mshg.db.Where("item_id = ?", itemID).First(&data).Error; err != nil {
		return err
	}

	if err := mshg.db.Delete(&data).Error; err != nil {
		return err
	}

	return nil
}
