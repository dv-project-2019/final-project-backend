package models

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

const FolderPath = "upload"
const LocalPort = "https://eatontimeapi.jumpingcrab.com"

type UserImage struct {
	gorm.Model
	UserID   uint   `gorm:"not null", json:"userId" `
	Filename string `gorm:"not null", json:"filename" `
	Url      string `gorm:"not null", json:"url"`
}

type UserImageGorm struct {
	db *gorm.DB
}

type UserImageService interface {
	CheckImageCreated(userID uint) bool
	DeleteImage(userID uint) error
	FindUserImageByID(userID uint) (*UserImage, error)
	CreateUserImage(file *multipart.FileHeader, userID uint) (*UserImage, error)
}

func NewUserImageService(db *gorm.DB) UserImageService {
	return &UserImageGorm{db}
}

func RandomStringRunes(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func (usImg *UserImageGorm) FindUserImageByID(userID uint) (*UserImage, error) {
	image := UserImage{}
	err := usImg.db.Where("user_id = ?", userID).First(&image).Error
	if err != nil {
		return nil, err
	}

	return &image, nil
}

func (usImg *UserImageGorm) CheckImageCreated(userID uint) bool {
	image := UserImage{}
	err := usImg.db.Where("user_id = ?", userID).First(&image).Error
	if err != nil {
		fmt.Println(err)
		return false
	}

	return true
}

func (usImg *UserImageGorm) DeleteImage(userID uint) error {
	image, err := usImg.FindUserImageByID(userID)
	if err != nil {
		return err
	}

	err = os.Remove(image.UserFilePath())
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
	}

	if err := usImg.db.Delete(&image).Error; err != nil {
		return err
	}

	return nil
}

func saveUserFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		log.Printf("saveFile - open file error: %v\n", err)
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		log.Printf("saveFile - create destination file error: %v\n", err)
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	if err != nil {
		log.Printf("saveFile - copy file error: %v\n", err)
	}
	return err
}

func (img *UserImage) UserFilePath() string {
	idStr := strconv.FormatUint(uint64(img.UserID), 10)
	return filepath.Join(FolderPath, "userImages", idStr, img.Filename)
}

func (usImg *UserImageGorm) CreateUserImage(file *multipart.FileHeader, userID uint) (*UserImage, error) {
	userIDStr := strconv.FormatUint(uint64(userID), 10)
	direction := filepath.Join(FolderPath, "userImages", userIDStr)
	if err := os.MkdirAll(direction, os.ModePerm); err != nil {
		return nil, err
	}

	tx := usImg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	randomFilename := RandomStringRunes(24)
	image := UserImage{
		UserID:   userID,
		Filename: randomFilename,
		Url:      LocalPort + "/userImage/" + userIDStr + "/" + randomFilename,
	}
	if err := tx.Create(&image).Error; err != nil {
		log.Printf("create image error: %v\n", err)
		tx.Rollback()
		return nil, err
	}

	if err := saveUserFile(file, image.UserFilePath()); err != nil {
		tx.Rollback()
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		log.Printf("transaction commit: %v\n", err)
		return nil, err
	}

	return &image, nil
}
